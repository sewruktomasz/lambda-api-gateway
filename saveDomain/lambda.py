import json
import pymongo
from bson import ObjectId
from JSONEncoder import *
import os
import logging
   
logger = logging.getLogger()

try:
    LOG_LEVEL = os.environ.get('LOG_LEVEL').upper()
    logger.setLevel(logging.getLevelName(LOG_LEVEL))
except:
    LOG_LEVEL = 'DEBUG'
    logger.setLevel(logging.getLevelName(LOG_LEVEL))
    
MONGO_SERVER = os.environ.get('MONGO_SERVER')
client = pymongo.MongoClient(MONGO_SERVER)
my_db = client.test
my_collection = my_db.domains

def is_in_collection(collection,id_from_body):
    find = collection.find_one({'_id':id_from_body})
    if(find is None):
        return 0
    else:
        return 1
    



def lambda_handler(event, context):
    try:
        body = json.loads(event['body'])
        if ('_id' in body):
            id_from_body=ObjectId(body['_id'])
            body.pop('_id')
            
            if(not is_in_collection(my_collection, id_from_body)):
                body['_id'] = id_from_body
                my_insert = my_collection.insert_one(body)
                my_inserted_id = JSONEncoder().encode(my_insert.inserted_id)
                logger.debug('Added new id to base, _id:'+my_inserted_id)
                return{
                'statusCode':200,
                'body': my_inserted_id
                }
            else:
                my_update = my_collection.update_one({'_id':id_from_body},{ "$set": body })
                logger.debug('Updated items in base:'+str(my_update.modified_count))
                return{
                    'statusCode':200,
                    'body':JSONEncoder().encode(my_update.modified_count)
                }
        else:
            my_insert = my_collection.insert_one(body)
            my_inserted_id = JSONEncoder().encode(my_insert.inserted_id)
            logger.debug('Added new item to base, _id:'+my_inserted_id)
            return{
                'statusCode':200,
                'body':my_inserted_id
        }
    except:
        logger.error('Damaged json!')
        return {
            'statusCode' : 400,
            'body':json.dumps('Damaged json')
        }

        
