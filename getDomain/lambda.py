import json
import pymongo
from bson import ObjectId
from JSONEncoder import *
import os
import logging
   
logger = logging.getLogger()

try:
    LOG_LEVEL = os.environ.get('LOG_LEVEL').upper()
    logger.setLevel(logging.getLevelName(LOG_LEVEL))
except:
    LOG_LEVEL = 'DEBUG'
    logger.setLevel(logging.getLevelName(LOG_LEVEL))
    

MONGO_SERVER = os.environ.get('MONGO_SERVER')
client = pymongo.MongoClient(MONGO_SERVER)
my_db = client.test
my_collection = my_db.domains





def lambda_handler(event, context):
    try:
        pathParameters = event['pathParameters']
        result = JSONEncoder().encode(my_collection.find_one({'domainId':pathParameters['domainId']}))
        if(str(result) == 'null'):
            logger.error('No domainId in base')
            return{
                'statusCode':404,
                'body':json.dumps({'status':404,
                                    'message':'Not found!'})
            }
        else:
            logger.debug('Success!')
            return{
                'statusCode':200,
                'body':result
                }

    except Exception as error:
        logger.error("Error: %s" % error)    
        return {
            'statusCode' : 400,
            'body':json.dumps('Something went wrong')
        }

        
