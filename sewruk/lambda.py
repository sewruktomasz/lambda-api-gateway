import json
import logging
import os
   
logger = logging.getLogger()

try:
    LOG_LEVEL = os.environ.get('LOG_LEVEL').upper()
    logger.setLevel(logging.getLevelName(LOG_LEVEL))
except:
    LOG_LEVEL = 'DEBUG'
    logger.setLevel(logging.getLevelName(LOG_LEVEL))


def status_200_body(user):
    result=json.dumps({
        'status':200,
        'user':user,
        'message':'I did it!'
    }, indent=2)
    return result

def status_403_body():
    result = json.dumps({
        'status': 403,
        'message': 'This method is forbidden'
        }, indent=2)
    return result

def status_400_body():
    result = json.dumps({
        'status': 400,
        'message': 'Bad Request'
        }, indent=2)
    return result


def lambda_handler(event, context):
    if(event['httpMethod'] == 'POST'):
        try:
            body_message=status_200_body(json.loads(event['body'])['user'])
            logger.debug('logged in, method:'+event['httpMethod'])
            return{
                'statusCode':200,
                'body': body_message
            }
            
        except:
            logger.error('body is damaged')
            return{
                'statusCode':400,
                'body':status_400_body()
            }
    
    else:
        logger.error('method:' + event['httpMethod'])
        return {
            'statusCode' : 403,
            'body':status_403_body()
        }